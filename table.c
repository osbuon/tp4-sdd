#include <stdlib.h>
#include <stdio.h>

#include "table.h"


#define M 256

Maj new_matrix() {
    Maj matrix;
    matrix.size = 0;
    matrix.table = malloc(sizeof(struct maj_element_t)*M);
    matrix.table->line = -1;
    return matrix;
}

void free_matrix(Maj * matrix) {
    for (unsigned int i = 0; i < matrix->size; i++) {
        struct cell_t * cour = matrix->table[i].list;
        while (cour != NULL) {
            struct cell_t * tmp = cour->next;
            free(cour);
            cour = tmp;
        }
    }
    free(matrix->table);
    matrix->size = 0;
    matrix->table = NULL;
}

struct maj_element_t * dicho(Maj matrix, int line) {
    unsigned int left = 0;
    unsigned int right = matrix.size;

    while (right != left) {
        unsigned int center = (right+left)/2;

        if (matrix.table[center].line < line) left = center;
        else right = center;
    }
    return matrix.table + left;
}

float get_matrix_value(Maj matrix, int i, int j) {
    float value = 0.;
    struct maj_element_t * line = dicho(matrix, i);

    if (line->line == i) {
        struct cell_t * column = line->list;
        while (column != NULL && column->column < j)
            column = column->next;
        
        if (column != NULL && column->column == j)
            value = column->value;
    }
    
    return value;

}

bool set_matrix_value(Maj * matrix, int i, int j, float value) {
    bool sucess = true;
    struct maj_element_t * line = dicho(*matrix, i);
    
    if (line->line == i) {
        struct cell_t ** column = &line->list;
        while (*column != NULL && (*column)->column < j)
            column = &(*column)->next;
        
        if ((*column)->column == j) {
            if (value != 0.)
                (*column)->value = value;
            else {
                struct cell_t * tmp = *column;
                *column = (*column)->next;
                free(tmp);
            }
        } else {
            struct cell_t * tmp = malloc(sizeof(struct cell_t));
            if (tmp != NULL) {
                tmp->column = j;
                tmp->value = value;
                tmp->next = (*column)->next;

                *column = tmp;
            } else sucess = false;
        }
    } else if (value != 0.) {
        for (struct maj_element_t * ptr = matrix->table + matrix->size; ptr >= line; ptr--)
            ptr[1] = ptr[0];
        
        line->line = i;
        line->list = malloc(sizeof(struct cell_t));
        matrix->size++;
        if (line->list != NULL) {
            line->list->column = j;
            line->list->value = value;
            line->list->next = NULL;
        } else sucess = false;
    }

    return sucess;
}

Maj load(FILE * file) {
    Maj matrix = new_matrix();
    if (matrix.table) {
        char s[M];        
        while (fgets(s,M,file)) {
            int k = 0, n = 0;
            char i[M];
            char j[M];
            char value[M];
            while (s[k] != '\0') {
                while (s[k] != ' ') {
                    i[k] = s[k];
                    k++;
                }
                k++;
                while (s[k] != ' ') {
                    j[n] = s[k];
                    k++;
                    n++;
                }
                k++;
                n = 0;
                while (s[k] != '\0') {
                    value[n] = s[k];
                    k++;
                    n++;
                }
                set_matrix_value(&matrix, atoi(i), atoi(j), atof(value));
            }
        }
    }
    return matrix;
}
