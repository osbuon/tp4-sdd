#ifndef __TABLE_H__
#define __TABLE_H__

#include <stdbool.h>


struct cell_t {
    int column;
    float value;
    struct cell_t * next;
};

struct maj_element_t {
    int line;
    struct cell_t * list;
};

typedef struct maj_t {
    unsigned int size;
    struct maj_element_t * table;
} Maj;

/*
 * Cree une nouvelle matrice vide.
 * @return la matrice. La propriete table vaut NULL si l'allocation a echouee.
*/
Maj new_matrix();

/*
 * Libere une matrice.
 * @param matrix un pointeur vers la matrice.
*/
void free_matrix(Maj * matrix);

/*
 * Recupere la valeur aux coordonnes i,j.
 * @param matrix la matrice.
 * @param i l'indice de ligne.
 * @param j l'indice de colonne.
 * @return la valeur.
*/
float get_matrix_value(Maj matrix, int i, int j);

/*
 * Definie la valeur aux coordonnes i,j.
 * @param matrix un pointeur vers la matrice.
 * @param i l'indice de ligne.
 * @param j l'indice de colonne.
 * @param value la valeur.
 * @return true si la valeur a bien ete aujoutee, false s'il y a eu une erreur.
*/
bool set_matrix_value(Maj * matrix, int i, int j, float value);

/*
 * Charge la matrice creuse depuis un fichier. La propriete table vaut NULL si l'allocation a echouee.
 * @param file le fichier contenant la matrice.
 * @return la matrice.
*/
Maj load(FILE * file);

#endif